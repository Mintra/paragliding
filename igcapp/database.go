package igcapp

import (
	"fmt"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//WebhookDB saves the info that we use to connect to a databse that stores webhook info
type WebhookDB struct {
	DatabaseURL    string `json:"databaseurl"`
	DatabaseName   string `json:"databasename"`
	CollectionName string `json:"collectionname"`
}

//TrackDB saves the info that we use to connect to a database that stores track info
type TrackDB struct {
	DatabaseURL    string `json:"databaseurl"`
	DatabaseName   string `json:"databasename"`
	CollectionName string `json:"collectionmame"`
}

//initialize the database
func (db *TrackDB) Init() {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	// none duplicates
	Index := mgo.Index{
		Key:        []string{"tracksourceurl"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = session.DB(db.DatabaseName).C(db.CollectionName).EnsureIndex(Index)
	if err != nil {
		panic(err)
	}
}

//adds a track, if successful it will be returned
func (db *TrackDB) Add(t TrackData) bool {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}

	allWasGood := true
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CollectionName).Insert(t)
	if err != nil {
		fmt.Errorf("Failed inserting a track to the database: %s", err.Error())
		return false
	}
	return allWasGood
}

//Count returns the count() of tracks in the DB
func (db *TrackDB) Count() int {

	session, err := mgo.Dial(db.DatabaseURL)

	count, err := session.DB(db.DatabaseName).C(db.CollectionName).Count()
	if err != nil {
		fmt.Printf("Failed getting the count from the database: %s", err.Error())
		return -1
	}
	return count
}

//Returns the trackwith the ID given, if the track was found
func (db *TrackDB) Get(key int) (TrackData, bool) {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	trackFound := true
	track := TrackData{}

	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{"id": key}).One(&track)
	if err != nil {
		trackFound = false
	}
	return track, trackFound

}

// returns all the tracks
func (db *TrackDB) GetAll() ([]TrackData, error) {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	tracks := []TrackData{}

	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{}).All(&tracks)
	if err != nil {
		return []TrackData{}, err
	}

	return tracks, nil
}

// returns a slice of the ID's used in the database
func (db *TrackDB) GetAllIDs() ([]int, error) {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	var tracks []TrackData

	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(nil).All(&tracks)
	if err != nil {
		return []int{}, nil
	}

	IDs := []int{}
	for _, track := range tracks {
		IDs = append(IDs, track.ID)
	}

	return IDs, nil
}

// returns the last track in the database
func (db *TrackDB) GetLast() (TrackData, error) {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	tracks, err := db.GetAll()
	if err != nil {
		fmt.Printf("Failed retrieving from the database", err.Error())
		return TrackData{}, err
	}
	return tracks[len(tracks)-1], nil
}

//The last track ID used is returned
func (db *TrackDB) GetLastID() int {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	lastTrack, err := db.GetLast()
	if err != nil {
		fmt.Printf("Failed getting the last ID", err.Error())
		return -1
	}
	return lastTrack.ID
}

// deletes all the tracks, also how many was deleted
func (db *TrackDB) DeleteAll() int {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	info, err := session.DB(db.DatabaseName).C(db.CollectionName).RemoveAll(bson.M{})
	if err != nil {
		fmt.Printf("Failed removing from the database", err.Error())
	}
	return info.Removed
}

// initialize the webhook
func (db *WebhookDB) Init() {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	Index := mgo.Index{
		Key:        []string{"url"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = session.DB(db.DatabaseName).C(db.CollectionName).EnsureIndex(Index)
	if err != nil {
		panic(err)
	}
}

// adds info about a webhook to the database
func (db *WebhookDB) Add(wh Webhook) bool {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{}).One(&wh)
	if err != nil {
		return false
	}
	return true
}

// returns the webhook with a given ID
func (db *WebhookDB) Get(ID int) Webhook {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	var wh Webhook
	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{"id": ID}).One(&wh)
	if err != nil {
		fmt.Println("Failed finding webhook with the ID given")
	}
	return wh
}

// webhook with the given Id is deleted and returned
func (db *WebhookDB) Delete(ID int) Webhook {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	var wh Webhook
	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{"id": ID}).One(&wh)
	if err != nil {
		fmt.Println("Failed finding any webhook with the ID")
	} else {
		err = session.DB(db.DatabaseName).C(db.CollectionName).Remove(bson.M{"id": ID})
	}
	return wh
}
