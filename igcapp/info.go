package igcapp

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

var (
	startTime time.Time
	nextID    int
	nextWBID  int
)

const (
	discordWebhookURL string = "https://discordapp.com/api/webhooks/506452331941068810/OxShJXfaY3_HygH8rNqzvsFOkIMgdw3tajlqUJzjKDu1U9J84bkUYW27oYQvHbNcfOwq"
)

func init() {
	startTime = time.Now()
	nextID = db.GetLastID() + 1
	nextWBID = webhookDB.GetLastID() + 1
}

//TrackData has the meta data, with the DB ID and source url
type TrackData struct {
	HDate          time.Time `json:"H_date"`
	Pilot          string    `json:"pilot"`
	Glider         string    `json:"glider"`
	GliderID       string    `json:"glider_id"`
	TrackLength    float64   `json:"track_length"`
	TrackSourceURL string    `json:"track_src_url"`
	ID             int       `json:"-"`
	Timestamp      int64     `json:"-"`
}

//Webhook has the url and a min trigger value for the webhook
type Webhook struct {
	URL             string `json:"webhookURL"`
	MinTriggerValue int    `json:"minTriggerValue`
	ID              int    `json:"-"`
	Timestamp       int64  `json:"-"`
}

//APIData has the info about the API
type APIData struct {
	Uptime  string `json:"uptime"`
	Info    string `json:"info"`
	Version string `json:"version"`
}

//FormatISO8601 formats time.Duration to a string according to the ISO8601 standard
func FormatISO8601(t time.Duration) string {
	seconds := int64(t.Seconds()) % 60
	minutes := int64(t.Minutes()) % 60
	hours := int64(t.Hours()) % 24

	totalHours := int64(t.Hours())
	days := (totalHours / 24) % 30 // this doesn't work, there's not 30 days in all months
	months := (totalHours / (24 * 30)) % 12
	years := totalHours / (24 * 30 * 12)

	return fmt.Sprint("P", years, "Y", months, "M", days, "DT", hours, "H", minutes, "M", seconds, "S")
}

//RemoveEmpty removes empty strings from an array
func RemoveEmpty(arr []string) []string {
	var newArray []string
	for _, str := range arr {
		if str != "" {
			newArray = append(newArr, str)
		}
	}

	return newArr
}

//TimeUpdate checks every 10 minutes for an update
func TimeUpdate() {
	delay := time.Minute * 1
	oldTrack := db.Count()

	for {
		if oldTrack != db.Count() {
			oldTrack = db.Count()
			UpdateDiscord()
		} else if rand.Intn(10) == 1 {
			NotifyMe()
		}

		time.Sleep(delay)
	}

}

//NotifyMe updates even if there's nothing updated
func NotifyMe() {
	content := make(map[string]string)
	content["content"] = "We have nothing new to show at this time\n"
	raw, _ := json.Marshal(content)

	_, err := http.Post(discordWebhookURL, "application/json", bytes.NewBuffer(raw))
	if err != nil {
		fmt.Println(err.Error())
	}
}

//UpdateDiscord sends an update to a discord channel
func UpdateDiscord() {
	content := make(map[string]string)
	content["content"] = "Database has been updated\n"
	raw, _ := json.Marshal(content)

	_, err := http.Post(discordWebhookURL, "application/json", bytes.NewBuffer(raw))
	if err != nil {
		fmt.Println(err.Error())
	}
}

//Min the smallest value is returned of a and b
func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
